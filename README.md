# GameBoy Library

A collection of GameBoy helper libraries for use with [gbc](https://gitlab.com/BonsaiDen/gbc-rs/) and [gbt](https://gitlab.com/BonsaiDen/gbc-rs/tree/master/gbt).

## Contents

- `core.gbc`: Low level abstractions useful for building games, see the [Example](examples/empty.gbc)
- `lz4.gbc`: Used for decompressing data created by `gbt compress`
- `mmp.gbc`: Used for playing back sound effects and music converted by `gbt lmms`
- `serial.gbc`: Asynchronous serial communication between two GameBoys.
- `sgb.gbc`: Basic abstraction for Super GameBoy support.

## Building 

To build and any of the examples simply install `gbc` and then run `gbc release` inside the examples directory.

This will produce finished ROM file in the example's `build` directory.

## License

Licensed under MIT.

